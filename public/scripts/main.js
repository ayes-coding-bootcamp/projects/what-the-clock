const dateAccessor = new Date();

const months = [
	"Jan", "Feb", "Mar", "Apr", "May", "Jun",
	"Jul", "Aug", "Sep", "Oct", "Nov", "Dec"
];

const weekdays = ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"];

function ClockElement(value = null, label = null) {
	this.value = value;
	this.label = label;
}

const clock = {
	elements: {
		year: new ClockElement(dateAccessor.getFullYear(), "Year"),
		month: new ClockElement(months[dateAccessor.getMonth()], "Month"),
		day: new ClockElement(weekdays[dateAccessor.getDay()], "Day"),
		hour: new ClockElement(dateAccessor.getHours(), "Hour"),
		minute: new ClockElement(dateAccessor.getMinutes(), "Minute"),
		second: new ClockElement(dateAccessor.getSeconds(), "Second"),
		period: new ClockElement(dateAccessor.getHours() >= 0 && dateAccessor.getHours() < 12 ? "am" : "pm", "Period")
	},

	createClock: () => {
		const newClock = document.createElement("section");
		newClock.id = "clock";

		let html = '';

		Object.keys(clock.elements).forEach(key => {
			const element = clock.elements[key];

			html += `
        <article>
          <h3 id="${key}Value">${String(element.value).padStart(2, '0')}</h3>
          <p id="${key}Label">${element.label}:</p>
        </article>
      `;
		});

		newClock.innerHTML = html;

		return newClock;
	},
};

function updateClock() {
	const dateAccessor = new Date();

	clock.elements.day.value = weekdays[dateAccessor.getDay()];
	clock.elements.hour.value = String(dateAccessor.getHours()).padStart(2, '0');
	clock.elements.minute.value = String(dateAccessor.getMinutes()).padStart(2, '0');
	clock.elements.second.value = String(dateAccessor.getSeconds()).padStart(2, '0');
	clock.elements.period.value = dateAccessor.getHours() >= 0 && dateAccessor.getHours() < 12 ? "am" : "pm";

	// update the clock element values in the HTML
	Object.keys(clock.elements).forEach(key => {
		const element = clock.elements[key];
		const valueEl = document.querySelector(`#${key}Value`);
		valueEl.innerText = String(element.value).padStart(2, '0');
	});
}

const initializeClock = () => {
	const container = document.querySelector("#clockContainer");
	container.prepend(clock.createClock());
	window.setInterval(updateClock, 1000);
};

initializeClock();
